﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {
        private readonly ICustomerRepository customerRepository;

        private readonly IDocumentRepository documentRepository;

        private readonly IIdentityProofer identityProofer;

        private readonly IAddressProofer addressProofer;

        public RegistrationApplication(
            ICustomerRepository customerRepository, 
            IDocumentRepository documentRepository, 
            IIdentityProofer identityProofer,
            IAddressProofer addressProofer)
        {
            this.customerRepository = customerRepository;
            this.documentRepository = documentRepository;
            this.identityProofer = identityProofer;
            this.addressProofer = addressProofer;
        }

        public async Task<IReadOnlyCustomer> CreateAccount(string email, string firstName, string lastName)
        {
            var newCustomer = new Customer(email, firstName, lastName);
            await customerRepository.SaveAsync(newCustomer);

            return newCustomer;
        }

        public async Task<IReadOnlyCustomer> GetAccount(string email)
        {
            return await customerRepository.GetAsync(email);
        }

        public async Task<bool> UploadIdentityProof(string email, byte[] isDocValidated)
        {
            var customer = await customerRepository.GetAsync(email);
            if (!identityProofer.Validates(customer, isDocValidated))
                return false;

            await documentRepository.SaveAsync(DocumentType.IdentityProof, email, isDocValidated);

            customer.IsIdentityVerified = true;
            await customerRepository.SaveAsync(customer);

            return true;
        }

        public async Task<bool> UploadAddressProof(string email, byte[] isDocValidated)
        {
            var customer = await this.customerRepository.GetAsync(email);
            if (!addressProofer.Validates(customer, out var address, isDocValidated))
                return false;

            await documentRepository.SaveAsync(DocumentType.AddressProof, email, isDocValidated);

            customer.Address = address;
            await this.customerRepository.SaveAsync(customer);

            return true;
        }
    }
}
