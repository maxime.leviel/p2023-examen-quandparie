﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.Domain
{
    public static class Extensions
    {
        public static decimal Product<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
            => source.Aggregate(1m, (accumulate, source) => accumulate * selector(source));

    }
}
